# CardShark

A rough library for building decks of cards, shuffling them, and determining
the high card in a given hand/trick.

The major impetus behind this was I wanted to easily compare cards in a set.
That required more structured card information. The `CardShark.Card` struct
is a bit verbose but allows straightfoward comparisons.

## Installation

The package can be installed by adding `card_shark` to your list of
dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:card_shark, "~> 0.1.0"}
  ]
end
```

## Example Usage

```elixir
alias CardShark.{Card, Deck, Compare}

deck = Deck.shuffled()
[%Card{}, %Card{}, ...]

{hand, remainder} = Deck.deal(deck, 5)
```
cards are played into an ordered list as different players play cards
```elixir
cards = [
  %Card{face: "10", suit: :diamonds},
  %Card{face: "jack", suit: :spades},
  %Card{face: "ace", suit: :diamonds}
]
```

which can then be compared to determine who won the round
```elixir
{%Card{face: "ace", suit: :diamonds}, index} = Compare.high_card(cards)
```

`Compare.high_card/2` also accepts an optional trump suit atom
```elixir
{%Card{face: "jack", suit: :spades}, index} = Compare.high_card(cards, :spades)
```

Documentation can be generated with
[ExDoc](https://github.com/elixir-lang/ex_doc) and published on
[HexDocs](https://hexdocs.pm). The docs can be found at
[https://hexdocs.pm/card_shark](https://hexdocs.pm/card_shark).
