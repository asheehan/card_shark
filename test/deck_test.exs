defmodule CardShark.DeckTest do
  use ExUnit.Case

  alias CardShark.{Card, Deck}

  doctest CardShark.Deck

  test "new returns an unshuffled deck" do
    assert [%Card{} | _] = Deck.new()
    assert length(Deck.new()) == 52
  end
end
