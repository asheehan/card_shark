defmodule CardShark.Card do
  @moduledoc "The basic card struct"

  defstruct [:suit, :face, :value]
end
